/**
 * 
 */

/**
 * @author Matthew Siu
 *
 */

import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.*;

public class RequestClient {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException{
		
		//Define the request buffer
		byte[] headerbuf = new byte[16];
		byte[] payload = new byte[16384];
		byte[] requestbuf = new byte[16400];
		byte[] studentidb = new byte[4];
				
		//Compose the request header
		headerbuf = composeHeaderbuf();
		
		//Compose the payload
		int studentID = Integer.parseInt(args[2].toString());
		ByteOrder.int2leb(studentID, studentidb, 0);
		
		//Append the byte of header and student ID 
		ByteBuffer brequestbuf = ByteBuffer.allocate(headerbuf.length + payload.length);
		brequestbuf.put(headerbuf);
		//brequestbuf.putInt(studentID);
		brequestbuf.put(studentidb);
		requestbuf = brequestbuf.array();
		
		//Compose the IP and Port
        InetAddress address = InetAddress.getByName(args[0]);
        int port = Integer.parseInt(args[1].toString());
        
        //Compose the packet 
        DatagramPacket packet = new DatagramPacket(requestbuf, requestbuf.length, address, port);
        
        //Server communication
        byte[] buf = new byte[16400];
		var responsepack = new DatagramPacket(buf, buf.length);
        responsepack = connectServer(packet, headerbuf);
              
        //Print the response
        if (responsepack.getPort() == 0) {
        	System.out.println("No response from server, please reconnect.");
        } else 
        	printResponse(responsepack, studentID);
   	}

	//Print the response from server
	private static void printResponse(DatagramPacket responsepack, int studentID) {
		
		byte[] responsedata = new byte[16384];
		byte[] secretlength = new byte[4];
		byte[] secretcode = new byte[16380];
		
		//Get the secret code length and secret code value
		responsedata = responsepack.getData();
		secretlength = Arrays.copyOfRange(responsedata, 16, 20);
		int sclength = ByteOrder.beb2int(secretlength,0);
		secretcode = Arrays.copyOfRange(responsedata, 20, 20 + sclength);
		String secode = StringUtils.byteArrayToHexString(secretcode);
		
		//Present the result
		System.out.println("Student ID: " + studentID);
        System.out.println("Secret Code Length: " + sclength);
        System.out.println("Secret Code : " + secode);
	}

	//Connect to server and send/receive packets
	private static DatagramPacket connectServer(DatagramPacket packet, byte[] headerbuf) throws IOException {
		
		//Define the response packet
		byte[] buf = new byte[16400];
		var responsepacket = new DatagramPacket(buf, buf.length);
		int timeout = 100;
		
		//Define a datagram socket
		try (DatagramSocket socket = new DatagramSocket()){
			for (int i=0 ; i<=3; i++) {
				//Send the packet to server
				socket.send(packet);
				
				//Set timeout value
				socket.setSoTimeout(timeout);
				timeout = timeout * 2;
				
				//Get the response
				try {
					socket.receive(responsepacket);
					//String rcvd = "rcvd from " + responsepacket.getAddress() + ":" + 
					//responsepacket.getPort() + ": "+ new String(responsepacket.getData(), 0, responsepacket.getLength());
					//System.out.println("Connection success: "+ rcvd);
					//Check the unique ID of the response
					byte[] responseheader = Arrays.copyOfRange(responsepacket.getData(), 0, 16);
					if (checkResponse(headerbuf,responseheader)) {
						socket.close();
						return responsepacket;
					} else {
						System.out.println("Unique ID mismatch! Retry count:" + i);
					}
				
				}
				catch (SocketTimeoutException e) {
		            // timeout exception.
					System.out.println("Timeout reached!!! Retry count:" + i);
				}
				if (i==3) {
					System.out.println("Retry limit reached, Operation failed!");
					responsepacket.setPort(0);
				}
			}
			//Close the socket
			socket.close();
			
		} catch (SocketException e) {
			e.printStackTrace();
		}
		return responsepacket;
	}

	
	private static boolean checkResponse(byte[] headerbuf, byte[] responseheader) {
		//int requestID = ByteOrder.leb2int(headerbuf, 0)	;
		//int responseID = ByteOrder.leb2int(responseheader, 0)	;
		//String reqhex = StringUtils.byteArrayToHexString(headerbuf);
		//String reshex = StringUtils.byteArrayToHexString(responseheader);
		//System.out.println(reqhex);
		//System.out.println(reshex);
		if (Arrays.equals(headerbuf, responseheader )== true) {
			System.out.println("Unique ID matched! Result in below : ");
			System.out.println();
			return true;
		} else {
			//System.out.println("Fail");
			return false;
		}
		
				
	}

	// Gather header information
	private static byte[] composeHeaderbuf() throws IOException {
						
		short ports;
		
		//Gather local IP address and port
		try(final DatagramSocket socket = new DatagramSocket()){
			  socket.connect(InetAddress.getByName("8.8.8.8"), 1213);
			  String ip = socket.getLocalAddress().getHostAddress();
			  int port = socket.getLocalPort();
			  
			  //Test if port > max value
			  if (port > Short.MAX_VALUE) {
			  		ports = 6666;
			  } else 
				  	ports = (short)port;
			  
			  //Remove dots from IP and store it in an integer
			  String newip = ip.replaceAll("\\.","");
			  int newipi = Integer.parseInt(newip);
			  
			  //Generate a random number
			  short ran = getrannum();
			  
			  //Gather system time
			  long ctime = System.nanoTime();
				  
			  //Combine them back into one single byte array
			  ByteBuffer headerbufferx = ByteBuffer.allocate(16);
			  headerbufferx.putInt(newipi);
			  headerbufferx.putShort(ports);
			  headerbufferx.putShort(ran);
			  headerbufferx.putLong(ctime);	
			  return headerbufferx.array();
			}
	}

	
	// Generate a short random number
	private static short getrannum() {
		Random rand = new Random();
		short rann = (short) rand.nextInt(89);
		rann += 10;
		return rann;
	}

}
